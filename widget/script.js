define(['jquery', 'lib/components/base/modal'], function ($, Modal) {

    var CustomWidget = function(){
        var self     = this,
            system   = self.system(),
            tempStatus = 0,
            newStatus = 0;

        /**
         * Открывает модальное окно для создания заявки и автоматической сделки
         */
        this.openModalAddPipelineApplication = function (t) {

            var pipelineID   = t.attr("data-pipelineid"),//ID воронки, в которой создается сделка с заявкой
                pipelineName = t.attr("data-pipelinename"),//название текущей воронки
                statusID     = t.attr("data-statusid"),//ID этапа, в котором создается сделка с заявкой
                leadID       = AMOCRM.constant("card_id"),//ID текущей сделки
                html         = '',
                modalCount   = $(".add-pipeline-application-modal-window");

            html += '<form method="post" enctype="multipart/form-data">';
            html += '<input type="hidden" name="MAX_FILE_SIZE" value="5000000" />';
            html += '<div style="margin-bottom:10px;margin-top:10px;overflow:hidden;">';
            html += '<div style="float:left;margin-right:10px;position:relative;overflow:hidden;cursor:pointer;border:1px solid #DBDEDF;border-radius:3px;height:36px;line-height:36px;text-align:center;width:130px;font-size:14px;">';
            html += '<label for="file_application"><input type="file" name="file_application" id="file_application" class="button-input" style="display:none;" multiple="multiple">';
            html += '<span style="color:#2e3640;font-weight:bold;cursor:pointer;">Загрузить файл</span>';
            html += '</label>';
            html += '</div>';
            html += '<span class="file-name-upload" style="display:block;float:left;height:36px;line-height:36px;"></span>';
            html += '</div>';
            html += '<div style="margin-bottom:10px;">';
            html += '<textarea id="comment_application" style="border:1px solid #dbdedf;padding:5px;width:97.4%;height:100px;" placeholder="Комментарий"></textarea>';
            html += '</div>';
            html += '<div>';
            html += '<button type="button" id="send_application" disabled="disabled" data-pipelineid="'+pipelineID+'" data-pipelinename="'+pipelineName+'" data-statusid="'+statusID+'" data-leadid="'+leadID+'" style="padding:5px 0;border:1px solid #dbdedf;text-align:center;border-radius:3px;color:#2e3640;width:100%;background:#fcfcfc;cursor:pointer;height:36px;font-weight:bold;font-size:14px;">Отправить</button>';
            html += '</div>';
            html += '</form>';

            //открываем модальное окно
            if (modalCount.length == 0) {
                modal = new Modal({
                    class_name: 'add-pipeline-application-modal-window',
                    init: function ($modal_body) {
                        var $this = $(this);
                        $modal_body
                            .trigger('modal:loaded') //запускает отображение модального окна
                            .html(html)
                            .trigger('modal:centrify') //настраивает модальное окно
                            .append('<span class="modal-body__close"><span class="icon icon-modal-close"></span></span>');
                    },
                    destroy: function () {}
                });
            }

        };

        /**
         * Открывает модальное окно для отправки результата заявки
         */
        this.openModalSendResult = function (t) {

            var statusID   = (newStatus == 0) ? parseInt(t.attr("data-statusid")) : newStatus,//ID статуса (чтобы знать с каким текстом ставить задачу)
                mainLeadID = t.attr("data-mainleadid"),//ID главной сделки, в ккоторую отправляем результат
                html       = '',
                modalCount = $(".send-result-application-modal-window");//объект модального окна

            //открываем мод. окно только в этапах "В работе" и "Расчет произведен"
            if (statusID == 21748639 || statusID == 21748684 || statusID == 142) {
                html += '<form method="post" enctype="multipart/form-data">';
                html += '<input type="hidden" name="MAX_FILE_SIZE" value="5000000" />';
                html += '<div style="margin-bottom:10px;margin-top:10px;overflow:hidden;">';
                html += '<div style="float:left;margin-right:10px;position:relative;overflow:hidden;cursor:pointer;border:1px solid #DBDEDF;border-radius:3px;height:36px;line-height:36px;text-align:center;width:130px;font-size:14px;">';
                html += '<label for="file_application"><input type="file" name="file_application" id="file_application" class="button-input" style="display:none;" multiple="multiple">';
                html += '<span style="color:#2e3640;font-weight:bold;cursor:pointer;">Загрузить файл</span>';
                html += '</label>';
                html += '</div>';
                html += '<span class="file-name-upload" style="display:block;float:left;height:36px;line-height:36px;"></span>';
                html += '</div>';
                html += '<div style="margin-bottom:10px;">';
                html += '<textarea id="comment_application" style="border:1px solid #dbdedf;padding:5px;width:97.4%;height:100px;" placeholder="Комментарий"></textarea>';
                html += '</div>';
                html += '<div>';
                html += '<button type="button" id="send_result_application" disabled="disabled" data-statusid="'+statusID+'" data-mainleadid="'+mainLeadID+'" style="padding:5px 0;border:1px solid #dbdedf;text-align:center;border-radius:3px;color:#2e3640;width:100%;background:#fcfcfc;cursor:pointer;height:36px;font-weight:bold;font-size:14px;">Отправить</button>';
                html += '</div>';
                html += '</form>';

                //открываем модальное окно
                if (modalCount.length == 0) {
                    modal = new Modal({
                        class_name: 'send-result-application-modal-window',
                        init: function ($modal_body) {
                            var $this = $(this);
                            $modal_body
                                .trigger('modal:loaded') //запускает отображение модального окна
                                .html(html)
                                .trigger('modal:centrify') //настраивает модальное окно
                                .append('<span class="modal-body__close"><span class="icon icon-modal-close"></span></span>');
                        },
                        destroy: function () {}
                    });
                }
            } else {
                alert("С этого этапа нельзя отправить результат по заявке.");
            }

        };

        /**
         * Загружает файл на сервер, создает заявку и автоматическую сделку
         */
        this.createApplicationLead = function (t) {
            var mainPipelineID    = (typeof AMOCRM.constant('card_element') != 'undefined' && typeof AMOCRM.constant('card_element').pipeline_id != 'undefined') ? AMOCRM.constant('card_element').pipeline_id : 0,//ID текущей воронки
                pipelineID        = t.attr("data-pipelineid"),//ID воронки, в которой создается сделка с заявкой
                pipelineName      = t.attr("data-pipelinename"),//название воронки, в которой будет создаваться дочерняя сделка
                statusID          = t.attr("data-statusid"),//ID этапа, в котором создается сделка с заявкой
                leadID            = t.attr("data-leadid"),//ID текущей сделки
                comment           = $("#comment_application").val(),//комментарий
                leadName          = AMOCRM.constant('card_element').name,//название сделки
                pipelines         = (typeof AMOCRM.constant('lead_statuses') != 'undefined') ? AMOCRM.constant('lead_statuses') : {},//хранит инфу о всех воронках и статусах аккаунта
                statusName        = '',//название статуса созданной автоматической сделки
                // fileData          = $('#file_application').prop('files')[0],//загружаемый файл
                fileData          = $('#file_application').prop('files'),//загружаемые файлы
                formData          = new FormData(),
                img               = '<div id="modal_loader" style="position:relative;position:absolute;top:0;left:0;width:100%;height:100%;background: rgba(255,255,255,.5);text-align:center;"><img style="position:absolute;top:50%;left:50%;" src="'+self.params.images_path+'/ajax_loader.gif"></div>',
                html              = '',
                responsibleUserID = (pipelineID == 1360207) ? 2683300 : 2683309,//ID ответственного сделки
                mainUserName      = (typeof AMOCRM.constant('card_element') != 'undefined' && AMOCRM.constant('card_element').main_user_name != 'undefined') ? AMOCRM.constant('card_element').main_user_name : '';//Имя ответственного в главной сделке
            
            //формируем массив для загрузки нескольких файлов
            for (var f in fileData) {
                formData.append("file[]", fileData[f]);
            }
            
            //добавляем картинку лоадера
            $(".add-pipeline-application-modal-window").find(".modal-body").css({"position": "relative"});
            $(".add-pipeline-application-modal-window").find(".modal-body").append(img);
            //формируем данные для отправки на сервер
            formData.append("action", "saveCreateApplicationLead");
            // formData.append("file", fileData);
            formData.append("mainPipelineID", mainPipelineID);
            formData.append("pipelineName", pipelineName);
            formData.append("pipelineID", pipelineID);
            formData.append("statusID", statusID);
            formData.append("leadID", leadID);
            formData.append("leadName", leadName);
            formData.append("leadLink", window.location.href);
            formData.append("comment", comment);
            formData.append("responsibleUserID", responsibleUserID);
            formData.append("mainUserName", mainUserName);
            console.log("form data", formData);

            //отправляем данные на сервер
            $.ajax({
                url: self.params.backend,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                async: false,
                type: 'post',
                success: function(data){
                    // console.log(data);
                    $(".add-pipeline-application-modal-window").find(".modal-body__close").trigger("click");
                    if (typeof data == 'object') {
                        //поиск названия этапа автоматической сделки по его ID
                        for (var s in pipelines[pipelineID].statuses) {
                            if (parseInt(pipelines[pipelineID].statuses[s].id) == parseInt(data.child_status_id)) {
                                statusName = pipelines[pipelineID].statuses[s].name;
                            }
                        }
                        //добавляем отчет о созданной заявки в виджет справа
                        html += '<div style="padding:5px 0;margin-bottom:5px;border-bottom:1px solid #DBDEDF;">';
                        html += '<p style="font-size:12px;"><span style="font-weight:bold;">Название сделки:</span> <a href="'+data.child_lead_link+'" target="_blank">'+data.child_lead_name+'</a></p>';
                        html += '<p style="font-size:12px;"><span style="font-weight:bold;">Отдел:</span> '+data.child_pipeline_name+'</p>';
                        html += '<p style="font-size:12px;"><span style="font-weight:bold;">Дата создания:</span> '+data.create_date+'</p>';
                        html += '<p style="font-size:12px;"><span style="font-weight:bold;">Текущий статус:</span> '+statusName+'</p>';
                        if (parseInt(data.return_date) != 0) {
                            html += '<p style="font-size:12px;"><span style="font-weight:bold;">Дата возврата:</span> '+data.return_date+'</p>';
                        }
                        html += '</div>';
                        $("#not_applications").remove();
                        $("#list_applications").find("#list_applications_sub").append(html);
                        alert("Заявка успешно создана.");
                    } else {
                        alert("Не удалось создать заявку.");
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        };

        /**
         * Загружает файл на сервер, создает примечание и задачу
         */
        this.createNoteTask = function (t) {
            var statusID   = (typeof AMOCRM.constant('card_element') != 'undefined' && typeof AMOCRM.constant('card_element').status != 'undefined' && newStatus == 0) ? parseInt(AMOCRM.constant('card_element').status) : newStatus,//ID статуса
                mainLeadID = t.attr("data-mainleadid"),//ID сделки, в которой создаем примечение и задачу
                comment    = $("#comment_application").val(),//комментарий
                // fileData   = $('#file_application').prop('files')[0],//загружаемый файл
                fileData   = $('#file_application').prop('files'),//загружаемые файлы
                img        = '<div id="modal_loader" style="position:relative;position:absolute;top:0;left:0;width:100%;height:100%;background: rgba(255,255,255,.5);text-align:center;"><img style="position:absolute;top:50%;left:50%;" src="'+self.params.images_path+'/ajax_loader.gif"></div>',
                formData   = new FormData(),
                leadID     = (typeof AMOCRM.constant('card_element') != 'undefined' && AMOCRM.constant('card_element').id != 'undefined') ? AMOCRM.constant('card_element').id : 0,//ID текущей сделки
                textTask   = (statusID == 21748639 || statusID == 21748684) ? 'Принять промежуточный расчет по заявке' : 'Принять финальный расчет по заявке',//текст создаваемой в сделке задачи
                textNote   = (statusID == 21748639 || statusID == 21748684) ? 'Отправлен промежуточный расчет по заявке' : 'Отправлен финальный расчет по заявке',
                statusNote = (statusID == 21748639 || statusID == 21748684) ? '0' : '1';

            //формируем массив для загрузки нескольких файлов
            for (var f in fileData) {
                formData.append("file[]", fileData[f]);
            }

            //добавляем картинку лоадера
            $(".send-result-application-modal-window").find(".modal-body").css({"position": "relative"});
            $(".send-result-application-modal-window").find(".modal-body").append(img);
            //формируем данные для отправки на сервер
            // formData.append("file", fileData);
            formData.append("action", "saveCreateNoteTask");
            formData.append("textTask", textTask);
            formData.append("textNote", textNote);
            formData.append("statusNote", statusNote);
            formData.append("mainLeadID", mainLeadID);
            formData.append("leadID", leadID);
            formData.append("leadLink", window.location.href);
            formData.append("comment", comment);
            //отправляем данные на сервер
            $.ajax({
                url: self.params.backend,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                async: false,
                type: 'post',
                success: function(data){
                    if (data == 'OK') {
                        $(".send-result-application-modal-window").find(".modal-body__close").trigger("click");
                        alert("Заявка успешно создана.");
                    } else {
                        alert("Не удалось создать заявку.");
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        };

        this.callbacks = {
            render: function() {

                // // Временно запрет для все остальных пользователей
                // if (system.amouser !== "neonproekt@leadfactor.ru") {
                //     return false;
                // }

                // Не проверять настройки, если на странице настроек
                if (system.area !== 'settings') {
                    try {
                        new URL(self.params.backend);
                    } catch (ex) {
                        console.error("Error - `backend` setting must be a valid URL: ", self.params.backend);
                        alert("Не указан правильный URL в настройках виджета.");
                        return false;
                    }
                }

                var mainPipeline    = (typeof AMOCRM.constant('main_pipeline') != 'undefined') ? AMOCRM.constant('main_pipeline') : 0,//ID главной воронки
                    currentPipeline = (typeof AMOCRM.constant('card_element') != 'undefined' && typeof AMOCRM.constant('card_element').pipeline_id != 'undefined') ? AMOCRM.constant('card_element').pipeline_id : 0,//ID текущей воронки
                    pipelines       = (typeof AMOCRM.constant('lead_statuses') != 'undefined') ? AMOCRM.constant('lead_statuses') : {},//хранит инфу о всех воронках и статусах аккаунта
                    renderHTML      = '',
                    html            = '',
                    leadID          = (typeof AMOCRM.constant('card_element') != 'undefined' && AMOCRM.constant('card_element').id != 'undefined') ? AMOCRM.constant('card_element').id : 0,//ID текущей сделки
                    sort            = 1000000000,//нужно для сортировки, чтобы найти 1 этап в воронке
                    statusID        = 0,//ID этапа воронки, в котором нужно создать сделку
                    statusName      = '',
                    statusLeadID    = (typeof AMOCRM.constant('card_element') != 'undefined' && typeof AMOCRM.constant('card_element').status != 'undefined') ? parseInt(AMOCRM.constant('card_element').status) : 0,
                    currentEntity   = AMOCRM.data.current_entity;

                if (currentEntity == 'leads') {

                    //находимся в главной воронке и формируем HTML код для нее
                    if (parseInt(mainPipeline) == parseInt(currentPipeline)) {
                        if (typeof pipelines != 'undefined' && Object.keys(pipelines) !== null && Object.keys(pipelines).length > 0) {
                            for (var p in pipelines) {
                                if (pipelines[p].is_main === false && pipelines[p].id != 1356214) {
                                    if (typeof pipelines[p].statuses != 'undefined' && Object.keys(pipelines[p].statuses) !== null && Object.keys(pipelines[p].statuses).length > 0) {
                                        sort = 1000000000;
                                        statusID = 0;
                                        for (var s in pipelines[p].statuses) {
                                            if (parseInt(pipelines[p].statuses[s].sort) < sort) {
                                                sort = parseInt(pipelines[p].statuses[s].sort);
                                                statusID = parseInt(pipelines[p].statuses[s].id);
                                            }
                                        }
                                    }
                                    renderHTML += '<button type="button" class="add-pipeline-application" data-pipelineid="'+pipelines[p].id+'" data-pipelinename="'+pipelines[p].name+'" data-statusid="'+statusID+'" style="margin-top:10px;padding:5px 0;border:1px solid #dbdedf;text-align:center;border-radius:3px;color:#2e3640;width:100%;background:#fcfcfc;cursor:pointer;font-weight:bold;height:36px;font-size:14px;">'+pipelines[p].label+'</button>';
                                }
                            }
                            //каркас HTML кода для вывода уже имеющихся заявок в БД
                            renderHTML += '<div id="wrap_list_applications" style="margin-top:10px;background:#FFF;padding:5px;">';
                            renderHTML += '<h3 style="border-bottom:1px solid;font-weight:bold;">Список заявок:</h3>';
                            renderHTML += '<div id="list_applications" style="max-height:300px;overflow-y:auto;margin-top:5px;">';
                            renderHTML += '<div id="list_applications_sub">';
                            renderHTML += '</div>';
                            renderHTML += '</div>';
                            renderHTML += '</div>';
                            //получим уже имеющиеся завяки из БД для текущей главной сделки
                            self.crm_post(
                                self.params.backend,
                                {action:"getDBApplications", pipelineID: currentPipeline, leadID: leadID},
                                function (data) {
                                    if (typeof data != 'undefined' && data.length > 0) {
                                        for (var i = 0; i < data.length; i++) {
                                            //поиск названия этапа автоматической сделки по его ID
                                            if (parseInt(data[i].child_status_id) != 143) {
                                                for (var s in pipelines[data[i].child_pipeline_id].statuses) {
                                                    if (parseInt(pipelines[data[i].child_pipeline_id].statuses[s].id) == parseInt(data[i].child_status_id)) {
                                                        statusName = pipelines[data[i].child_pipeline_id].statuses[s].name;
                                                    }
                                                }
                                            } else {
                                                statusName = 'Отклонено';
                                            }
                                            //добавляем отчет о созданной заявки в виджет справа
                                            html += '<div style="padding:5px 0;margin-bottom:5px;border-bottom:1px solid #DBDEDF;">';
                                            html += '<p style="font-size:12px;"><span style="font-weight:bold;">Название сделки:</span> <a href="'+data[i].child_lead_link+'" target="_blank">'+data[i].child_lead_name+'</a></p>';
                                            html += '<p style="font-size:12px;"><span style="font-weight:bold;">Отдел:</span> '+data[i].child_pipeline_name+'</p>';
                                            html += '<p style="font-size:12px;"><span style="font-weight:bold;">Дата создания:</span> '+data[i].child_create_date+'</p>';
                                            html += '<p style="font-size:12px;"><span style="font-weight:bold;">Текущий статус:</span> '+statusName+'</p>';
                                            if (parseInt(data[i].child_return_date) != 0) {
                                                html += '<p style="font-size:12px;"><span style="font-weight:bold;">Дата возврата:</span> '+data[i].child_return_date+'</p>';
                                            }
                                            html += '</div>';
                                        }
                                    } else {
                                        html += '<p id="not_applications" style="text-align:center;margin-top:10px;margin-bottom:5px;font-weight:bold;">Заявок пока нет.</p>';
                                    }
                                    $("#list_applications").find("div").append(html);
                                },
                                'json',
                                function (error) {
                                    console.log(error);
                                }
                            );
                        }
                    } else {//находимся в автоматически созданной сделке

                        //формируем кнопку "Отправить результат"
                        renderHTML += '<button type="button" id="send_result" data-statusid="'+statusLeadID+'" style="margin-top:10px;padding:5px 0;border:1px solid #dbdedf;text-align:center;border-radius:3px;color:#2e3640;width:100%;background:#fcfcfc;cursor:pointer;font-weight:bold;height:36px;font-size:14px;">Отправить результат</button>';

                        //получаем с сервера данные о заявке из текущей сделки
                        self.crm_post(
                            self.params.backend,
                            {action:"getDBApplication", pipelineID: currentPipeline, leadID: leadID},
                            function (data) {
                                if (typeof data != 'undefined' && Object.keys(data) !== null && Object.keys(data).length > 0) {
                                    //добавляем к кнопке "Отправить результат" атрибут ID главной сделки
                                    $("#send_result").attr("data-mainleadid", data.main_lead_id);
                                }
                            },
                            'json',
                            function (error) {
                                console.log(error);
                            }
                        );

                    }
                    
                }

                self.render_template({
                    caption: {
                        class_name: '',
                    },
                    body: '',
                    render: renderHTML
                });
                return true;
            },
            init: function() {
                return true;
            },
            bind_actions: function() {

                //открывает модальное окно для создания заявки и автоматической сделки
                $("body").on("click", ".add-pipeline-application", function () {
                    self.openModalAddPipelineApplication($(this));
                });

                //делает активной кнопку "Отправить" в мод. окне при создании заявки и автоматической сделки
                $("body").on("change", "#file_application", function () {
                    $("#send_application, #send_result_application").prop("disabled", false);
                    // var fileName = $('#file_application').prop('files')[0];
                    var fileName = $('#file_application').prop('files'),
                        text     = '';
                    for (var f in fileName) {
                        if (typeof fileName[f].name != 'undefined' && fileName[f].name != 'item') {
                            text += fileName[f].name + ', ';
                        }
                    }
                    $(".file-name-upload").text(text.substr(0, text.length - 2));
                });

                //загружает файл на сервер, создает заявку и автоматическую сделку
                $("body").on("click", "#send_application", function () {
                    self.createApplicationLead($(this));
                });

                //открывает модальное окно для отправки результата заявки
                $("body").on("click", "#send_result", function () {
                    self.openModalSendResult($(this));
                });

                //загружает файл на сервер, создает примечание и задачу
                $("body").on("click", "#send_result_application", function () {
                    self.createNoteTask($(this));
                });

                /**
                 * Отлов изменения этапа в карточке сделки
                 */
                //клик по эл-ту LI во время изменения этапа в карточке сделки
                $("body").on("click", ".pipeline-select__dropdown__item", function () {
                    var t = $(this);
                    tempStatus = parseInt(t.find('input').val());
                });
                
                //ловим ID нового этапа при сохранении сделки
                $("body").on("click", "#save_and_close_contacts_link", function () {
                    newStatus = tempStatus;
                });
                /**
                 * END Отлов изменения этапа в карточке сделки
                 */

                return true;
            },
            settings: function(){

                return true;
            },
            onSave: function(){
                return true;
            },
            destroy: function(){},
            contacts: {
                selected: function(){}
            },
            leads: {
                selected: function(){}
            },
            tasks: {
                selected: function(){}
            }
        };

        return this;

    };

    return CustomWidget;

});