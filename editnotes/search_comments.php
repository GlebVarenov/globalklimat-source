<?php

/**
 * Utility to search for lead comments created by this widget (file download links)
 */


error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

// подключение библиотек
require_once __DIR__ . '/vendor/autoload.php';

// Загружаем настройки из файла ".env" (или ".env.testing", если выполнение
// происходит из тестов PHPUnit)
$dotenv = Dotenv\Dotenv::createImmutable(
  __DIR__ . "/../backend",
  getenv('APP_ENV') === 'test' ? '.env.testing' : '.env'
);
$dotenv->load();
$dotenv
  ->required([
    "USER_LOGIN", "USER_HASH", "AMO_SUBDOMAIN",
  ])
  ->notEmpty();

try {
  $amo = new \AmoCRM\Client(
    getenv('AMO_SUBDOMAIN'),
    getenv('USER_LOGIN'),
    getenv('USER_HASH')
  );

  $offset = 0;
  do {
    $batch = $amo->note->apiList(
      [
        'type' => 'lead',
        'note_type' => 4,
        'limit_rows' => 100,
        'limit_offset' => $offset,
      ]
    );
    foreach ($batch as $i => $note) {
      if (strpos($note['text'], 'Ссылка на файл:') !== false) {
        printf(
          "%s\t%d\t%s\n",
          $note['id'],
          $offset + $i,
          str_replace(["\r", "\n"], "$", $note['text'])
        );
      }
    }
    $results_count = count($batch);
    $offset += $results_count;
    usleep($constSleep);
  } while ($results_count === 100);

  $last_one = $batch[count($batch) - 1];
  printf("Last date_create: %s; id: %s", $last_one['date_create'], $last_one['id']);
} catch (\Exception $e) {
  // var_dump($e);
  fwrite(STDERR, print_r($e, true));
}
