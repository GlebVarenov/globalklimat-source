<?php

/**
 * Вносит правки в существующие примечания к сделкам - меняет URL ссылки на скачивание
 */

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

// подключение библиотек
require_once __DIR__ . "/vendor/autoload.php";

// Загружаем настройки из файла ".env" (или ".env.testing", если выполнение
// происходит из тестов PHPUnit)
$dotenv = Dotenv\Dotenv::createImmutable(
    __DIR__ . "/../backend",
    getenv('APP_ENV') === 'test' ? '.env.testing' : '.env'
);
$dotenv->load();
$dotenv
    ->required([
        "USER_LOGIN", "USER_HASH", "AMO_SUBDOMAIN",
    ])
    ->notEmpty();

try {
    $amo = new \AmoCRM\Client(
        getenv('AMO_SUBDOMAIN'),
        getenv('USER_LOGIN'),
        getenv('USER_HASH')
    );

    // $handle = fopen("results.txt", "r");
    $handle = STDIN;
    if ($handle) {
        // read line
        while (($line = fgets($handle)) !== false) {
            // process the line read.
            if (strlen(trim($line)) === 0) {
                continue;
            }

            // read note id
            $matches = [];
            preg_match("/^(\d+)\s/", $line, $matches);
            if (!isset($matches[1]) || !is_numeric($matches[1])) {
                throw new \Exception("Couldn't find an ID in an input line: $line", 1);
            }
            $note_id = $matches[1];

            // load note by id
            $notes = $amo->note->apiList(
                [
                    'id' => $note_id,
                ]
            );

            // read text content of note
            $text = $notes[0]['text'];

            // replace in text content
            $text = str_replace(
                "https://globalclimat.acerbic.tech/globalklimat/index.php",
                "https://globalclimat.trulyacerbic.stream/globalklimat/index.php",
                $text
            );

            echo $note_id . "\n";

            // update note text content by id
            $note = $amo->note;
            $note['text'] = $text;

            sleep(1);
            $success = $note->apiUpdate((int) $note_id);
            var_dump($success);
            sleep(1);
        }

        fclose($handle);
    } else {
        throw new \Exception("Failed to open input");
    }
} catch (\Exception $e) {
    fprintf(STDERR, print_r($e));
}
