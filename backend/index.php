<?php

header("Access-Control-Allow-Origin: *");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require dirname(__FILE__) . '/functions.php';
require dirname(__FILE__) . '/config.php';
require dirname(__FILE__) . '/db.php';
require dirname(__FILE__) . '/amocrm.php';

//выгрузка файла в браузер
if (isset($_GET['filename'])) {

    $downloadFile = (string) $_GET['filename'];
    if (preg_match("/^[a-zA-Z0-9_]+\.[a-zA-Z0-9_]*$/", $downloadFile) !== 1) {
        exit;
    }

    $filepathname = dirname(__FILE__) . '/files/' . $downloadFile;
    // if (file_exists(dirname(__FILE__) . '/files/' . $downloadFile)) {
    if (file_exists($filepathname)) {
        // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
        // если этого не сделать файл будет читаться в память полностью!
        if (ob_get_level()) {
            ob_end_clean();
        }
        // заставляем браузер показать окно сохранения файла
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $downloadFile);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepathname));
        // читаем файл и отправляем его пользователю
        readfile($filepathname);
        exit;
    }
}

if (isset($_POST['action'])) {

    $error = '';
    $action = (string) $_POST['action'];

    //сохраняет файл заявки на сервере, сохраняет данные о ней в БД, создает автоматическую сделку
    if ($action == 'saveCreateApplicationLead') {

        $mainPipelineID    = (int) $_POST['mainPipelineID']; //ID главной воронки, из которой создается заявка
        $mainLeadID        = (int) $_POST['leadID']; //ID текущей сделки
        $childPipelineID   = (int) $_POST['pipelineID']; //ID воронки, в которой создается автоматическая сделка
        $childPipelineName = (string) $_POST['pipelineName']; //ID воронки, в которой создается автоматическая сделка
        $childStatusID     = (int) $_POST['statusID']; //ID этапа, в котором создается автоматическая сделка
        $comment           = (string) $_POST['comment']; //комментарий к заявке
        $mainLeadLink      = (string) $_POST['leadLink']; //ссылка на текущую сделку
        $mainLeadName      = (string) $_POST['leadName']; //название текущей сделки
        $file              = (array) $_FILES['file']; //загружаемый файл
        $responsibleUserID = (int) $_POST['responsibleUserID']; //ID ответственного сделки
        $files             = array(); //массив загружаемых файлов
        $fileNames         = array(); //массив имен загружаемых файлов
        $mainUserName      = (string) $_POST['mainUserName']; //Имя ответственного в главной сделке

        //формируем нужный массив файлов
        foreach ($file as $key => $fl) {
            foreach ($fl as $k => $f) {
                $files[$k][$key] = $f;
            }
        }

        foreach ($files as $file) {
            //сохраняем файл заявки на сервере
            if ($file['error'] > 0) {
                switch ($file['error']) {
                    case 1:
                        $error = 'Размер файла больше 5МБ.';
                        break;
                    case 2:
                        $error = 'Размер файла больше 5МБ.';
                        break;
                    case 3:
                        $error = 'Загружена только часть файла.';
                        break;
                    case 4:
                        $error = 'Файл не загружен.';
                        break;
                    case 6:
                        $error = 'Загрузка невозможна: не задан временный каталог.';
                        break;
                    case 7:
                        $error = 'Загрузка не выполнена: невозможна запись на диск.';
                        break;
                }
            } else {
                //проверим наличие каталога файлов
                if (!file_exists(dirname(__FILE__) . '/files')) {
                    mkdir(dirname(__FILE__) . '/files', 0777);
                }
                $extension = explode(".", $file['name']);
                $extension = $extension[1];
                $uploadDir = dirname(__FILE__) . '/files/';
                $fileName = uniqid() . '_' . $mainLeadID . '.' . $extension;
                //если файл загружен методом POST
                if (is_uploaded_file($file['tmp_name'])) {
                    //сохраняем файл на сервере
                    if (!move_uploaded_file($file['tmp_name'], $uploadDir . $fileName)) {
                        $error = 'Проблема: невозможно переместить файл в каталог назначения!';
                    } else {
                        $fileNames[] = $fileName;
                    }
                } else {
                    $error = 'Проблема: возможно возникла атака через загрузку файла на сервер!';
                }
            }
        }

        if (!empty($fileNames)) {
            if (auth()) {
                $time = time();
                //создаем сделку
                $createLeadInfo = array();
                $childLeadName = 'Заявка по Сделке_' . $mainLeadName;
                if ($createLeadInfo = createLead($childLeadName, $childStatusID, $time, $responsibleUserID)) {
                    $childLeadID = $createLeadInfo[0]['id'];
                    //создаем примечание к сделке
                    $createNoteInfo = array();
                    $note = 'Ссылка на основную сделку: ' . $mainLeadLink . "\r\n";
                    $note .= 'Передал заявку: ' . $mainUserName . "\r\n";
                    foreach ($fileNames as $fileName) {
                        $note .= 'Ссылка на файл: ' . getenv('SERVER_SCRIPT') . '?filename=' . $fileName . "\r\n";
                    }
                    if (!empty($comment)) $note .= 'Комментарий: ' . $comment . "\r\n";
                    if ($createNoteInfo = createNote($childLeadID, $note, $time)) {
                        //сохраняем данные в БД
                        if (saveLeadsApplications($mainPipelineID, $mainLeadID, $childPipelineID, $childPipelineName, $childStatusID, $childLeadID, $childLeadName, $time, $subdomain)) {
                            $error = array(
                                "child_pipeline_name" => $childPipelineName,
                                "child_lead_name" => $childLeadName,
                                "child_lead_link" => "https://" . $subdomain . ".amocrm.ru/leads/detail/" . $childLeadID,
                                "create_date" => date("d.m.Y H:i:s", $time),
                                "child_status_id" => $childStatusID,
                                "return_date" => 0
                            );
                        }
                    }
                } else {
                    $error = 'Не удалось создать автоматическую сделку';
                }
            }
        }
    }

    //получает уже имеющиеся заявки из БД для главной сделки
    if ($action == 'getDBApplications') {

        $pipelineID = (int) $_POST['pipelineID'];
        $leadID     = (int) $_POST['leadID'];
        $error = getDBApplications($pipelineID, $leadID);
    }

    // получает данные о заявке для автоматически созданной сделки
    if ($action == 'getDBApplication') {

        $childPipelineID = (int) $_POST['pipelineID'];
        $childLeadID     = (int) $_POST['leadID'];
        $error = getDBApplication($childPipelineID, $childLeadID);
    }

    // загружает файл, создает примечание и задачу при отправке заявки в главную
    // сделку
    if ($action == 'saveCreateNoteTask') {

        $mainLeadID   = (int) $_POST['mainLeadID']; //ID главной сделки
        $leadID       = (int) $_POST['leadID']; //ID текущей сделки
        $comment      = (string) $_POST['comment']; //комментарий к заявке
        $mainLeadLink = (string) $_POST['leadLink']; //ссылка на текущую сделку
        $textTask     = (string) $_POST['textTask']; //текст задачи
        $textNote     = (string) $_POST['textNote']; //текст примечания
        $file         = (array) $_FILES['file']; //загружаемый файл
        $statusNote   = (int) $_POST['statusNote']; //если $statusNote = 1, то обновляем время возврата, т.к. отправлен финальный файл расчета
        $files        = array(); //массив загружаемых файлов
        $fileNames    = array(); //массив имен загружаемых файлов

        //формируем нужный массив файлов
        foreach ($file as $key => $fl) {
            foreach ($fl as $k => $f) {
                $files[$k][$key] = $f;
            }
        }

        //сохраняем файл заявки на сервере
        foreach ($files as $file) {
            if ($file['error'] > 0) {
                switch ($file['error']) {
                    case 1:
                        $error = 'Размер файла больше 5МБ.';
                        break;
                    case 2:
                        $error = 'Размер файла больше 5МБ.';
                        break;
                    case 3:
                        $error = 'Загружена только часть файла.';
                        break;
                    case 4:
                        $error = 'Файл не загружен.';
                        break;
                    case 6:
                        $error = 'Загрузка невозможна: не задан временный каталог.';
                        break;
                    case 7:
                        $error = 'Загрузка не выполнена: невозможна запись на диск.';
                        break;
                }
            } else {
                $extension = explode(".", $file['name']);
                $extension = $extension[1];
                $uploadDir = dirname(__FILE__) . '/files/';
                $fileName = uniqid() . '_' . $mainLeadID . '.' . $extension;
                //если файл загружен методом POST
                if (is_uploaded_file($file['tmp_name'])) {
                    //сохраняем файл на сервере
                    if (!move_uploaded_file($file['tmp_name'], $uploadDir . $fileName)) {
                        $error = 'Проблема: невозможно переместить файл в каталог назначения!';
                    } else {
                        $fileNames[] = $fileName;
                    }
                } else {
                    $error = 'Проблема: возможно возникла атака через загрузку файла на сервер!';
                }
            }
        }

        if (!empty($fileNames)) {
            if (auth()) {
                //получаем инфу по главной сделке
                $currentLeadInfo = getCurrentLeadInfo($mainLeadID);
                $responsibleUserID = $currentLeadInfo[0]['responsible_user_id'];
                $time = time();
                //создаем примечание в главной сделке
                $createNoteInfo = array();
                $note = $textTask . "\r\n";
                $note .= 'Ссылка на сделку: ' . $mainLeadLink . "\r\n";
                foreach ($fileNames as $fileName) {
                    $note .= 'Ссылка на файл: ' . getenv('SERVER_SCRIPT') . '?filename=' . $fileName . "\r\n";
                }
                if (!empty($comment)) $note .= 'Комментарий: ' . $comment . "\r\n";
                if ($createNoteInfo = createNote($mainLeadID, $note, $time)) {
                    //создаем задачу в сделке
                    if ($createTaskInfo = createTask($mainLeadID, $textTask, $responsibleUserID)) {
                        //создаем примечание в дочерней сделке
                        if ($createNoteInfo = createNote($leadID, $textNote, $time)) {
                            //если отправлен финальный файл, то обновляем время возврата файла в БД
                            if ($statusNote == 1) {
                                updateTimeFile($mainLeadID, $leadID, $time);
                            }
                            $error = "OK";
                        }
                    }
                }
            }
        }
    }

    if (!empty($error) && !is_array($error) && $error != 'OK') {
        recordLog("000", $error);
    }

    echo json_encode($error);
    die;
}
