<?php

/**
 * Конфигурационный файл
 */

require_once __DIR__ . "/vendor/autoload.php";

// Загружаем настройки из файла ".env"
if (file_exists(__DIR__ . '/.env')) {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $dotenv
        ->required([
            "USER_LOGIN", "USER_HASH", "AMO_SUBDOMAIN",
            "DB_FILE",
            "SERVER_SCRIPT"
        ])
        ->notEmpty();
}

//Массив с параметрами, которые нужно передать методом POST к API системы
$user = array(
    'USER_LOGIN' => getenv('USER_LOGIN'),
    'USER_HASH' => getenv('USER_HASH'),
);

//Наш аккаунт - поддомен
$subdomain = getenv('AMO_SUBDOMAIN');

//коды ошибок
$errors = array(
    301 => 'Moved permanently',
    400 => 'Bad request',
    401 => 'Unauthorized',
    403 => 'Forbidden',
    404 => 'Not found',
    500 => 'Internal server error',
    502 => 'Bad gateway',
    503 => 'Service unavailable'
);

$constSleep = 1200000; //период прерывания запроса к AmoCRM, равный 1.2 сек.
$timeCorrect = 60 * 3; //время корректирования для обновления данных в AmoCRM
