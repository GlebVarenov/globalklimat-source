<?php

function debug($arr)
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

function recordLog($code = '', $error)
{
    //проверить размер файл, и, если он больше 100Мб, то отправить письмо админу
    if (file_exists(dirname(__FILE__) . '/log.txt')) {
        if (filesize(dirname(__FILE__) . '/log.txt') > 104857600) {
            error_log('Размер файла log.txt превышает 100Мб', 1, 'stas.gavrichenko@mail.ru');
        }
    }

    //записать лог в файл
    file_put_contents('log.txt', 'Код ошибки - ' . $code . PHP_EOL . date("d-m-Y H:s") . ' - ' . $error . PHP_EOL . PHP_EOL, FILE_APPEND);
}

function isleep(){
    global $constSleep;
    usleep($constSleep);
}