<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once "config.php";
require_once "db.php";

// stub for recordLog from "functions.php"
/**
 * Call arguments log
 */
$recordLog_called = [];
function recordLog($code = '', $error)
{
    global $recordLog_called;
    $recordLog_called[] = [$code, $error];
}


final class DbTest extends TestCase
{
    private function assertRecordLogEmpty(): void
    {
        global $recordLog_called;
        if (!empty($recordLog_called[0])) {
            // FIXME: throw some CustomTestFailedException
            $this->assertEmpty($recordLog_called, "recordLog was called with args: " .
                print_r($recordLog_called[count($recordLog_called) - 1], true));
        }
    }

    public static function setUpBeforeClass(): void
    {
        $db = connectDB();
        $db->exec(
            "CREATE TABLE IF NOT EXISTS `leads_applications` (
                `id` INTEGER PRIMARY KEY,
                `main_pipeline_id` unsigned int(11) NOT NULL, -- ID главной воронки, из которой создавали заявку и автоматическую сделку
                `main_lead_id` unsigned int(11) NOT NULL, -- ID сделки, которой является главной по отношению к автоматической сделке
                `child_pipeline_id` unsigned int(11) NOT NULL, -- ID воронки, в которой создали автоматическую сделку
                `child_pipeline_name` varchar(255) NOT NULL,
                `child_status_id` unsigned int(11) NOT NULL, -- ID этапа, в котором создается автоматическая сделка
                `child_lead_id` unsigned int(11) NOT NULL, -- ID автоматически созданной сделки
                `child_lead_name` varchar(255) NOT NULL, -- название автоматической сделки
                `child_lead_link` varchar(255) NOT NULL,
                `child_create_date` unsigned int(11) NOT NULL, -- дата создания автоматической сделки
                `child_return_date` unsigned int(11) NOT NULL -- дата возврата заявки, если она обработана
            );"
        );
    }

    public function tearDown(): void
    {
        // purge db
        // TODO: how to prevent execution on prod db?
        $db = connectDB();
        $stmt = $db->prepare("DELETE FROM `leads_applications` WHERE 1;");
        $stmt->execute();
    }

    public function setUp(): void
    {
        global $recordLog_called;
        $recordLog_called = [];
    }

    public function testCanConnectToDb(): void
    {
        $db = connectDB();

        $this->assertNotNull($db, "connectDB must return a non-null entity");
        $this->assertRecordLogEmpty();
    }

    public function testCanSaveLeadsApplications(): void
    {
        $result = saveLeadsApplications(1111, 101, 2222, 'child_pipeline', 201, 202, 'child_lead', time(), 'subdomain');

        $this->assertRecordLogEmpty();
        $this->assertTrue($result, "saveLeadsApplications returned false");
    }

    public function testCanGetDbApplications(): void
    {
        saveLeadsApplications(12345, 321, 2222, 'child_pipeline_2', 201, 202, 'child_lead_2', time(), 'subdomain');
        saveLeadsApplications(12345, 321, 3333, 'child_pipeline_3', 201, 203, 'child_lead_3', time(), 'subdomain');

        $apps = getDBApplications(12345, 321);

        $this->assertIsArray($apps, "Must return an array");
        $this->assertNotEmpty($apps, "Must have some results");
        $this->assertEquals(2, count($apps));
        usort($apps, function ($a, $b) {
            return $a['child_pipeline_id'] - $b['child_pipeline_id'];
        });
        $this->assertEquals(2222, $apps[0]['child_pipeline_id']);
        $this->assertEquals('child_pipeline_2', $apps[0]['child_pipeline_name']);
        $this->assertEquals('https://subdomain.amocrm.ru/leads/detail/202', $apps[0]['child_lead_link']);
        $this->assertRecordLogEmpty();
    }

    public function testGetDbApplication(): void
    {
        saveLeadsApplications(12345, 3219, 2229, 'child_pipeline_2', 201, 2029, 'child_lead_2', time(), 'subdomain');

        $app = getDBApplication(2229, 2029);

        $this->assertNotEmpty($app, "Must have some results");
        $this->assertEquals(3219, $app['main_lead_id']);
        $this->assertRecordLogEmpty();
    }

    public function testChangeLeadStatus(): void
    {
        saveLeadsApplications(12345, 321, 2222, 'child_pipeline_2', 201, 202, 'child_lead_2', time(), 'subdomain');

        changeLeadStatus(999, 202);

        $apps = getDBApplications(12345, 321);
        $this->assertNotEmpty($apps);
        $this->assertEquals(999, $apps[0]['child_status_id']);
        $this->assertRecordLogEmpty();
    }

    public function testGetMainLeadId(): void
    {
        saveLeadsApplications(12345, 321, 2222, 'child_pipeline_2', 201, 202, 'child_lead_2', time(), 'subdomain');

        $mid = (getMainLeadID(202))['main_lead_id'];

        $this->assertEquals(321, $mid);
    }

    public function testUpdateTimeFile(): void
    {
        saveLeadsApplications(12345, 321, 2222, 'child_pipeline_2', 201, 202, 'child_lead_2', 100200, 'subdomain');

        updateTimeFile(321, 202, 200300);

        $apps = getDBApplications(12345, 321);
        $expected_date = date("d.m.Y H:i:s", 200300);
        $this->assertEquals($expected_date, $apps[0]['child_return_date']);
        $this->assertRecordLogEmpty();
    }

    public function testDeleteLeadDb(): void
    {
        saveLeadsApplications(12345, 321, 2222, 'child_pipeline_2', 201, 202, 'child_lead_2', time(), 'subdomain');
        saveLeadsApplications(12345, 321, 3333, 'child_pipeline_3', 201, 203, 'child_lead_3', time(), 'subdomain');
        $apps = getDBApplications(12345, 321);
        $this->assertCount(2, $apps);

        deleteLeadDB(202);

        $apps = getDBApplications(12345, 321);
        $this->assertCount(1, $apps);
        $this->assertEquals(3333, $apps[0]['child_pipeline_id']);
    }
}
