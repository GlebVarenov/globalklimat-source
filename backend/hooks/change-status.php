<?php

/**
 * Изменяет этап сделки и дату ее возврата в БД при изменении сделки клиентом в 
 * AmoCRM если она перешла в этап "Закрыта и не реализована"
 */

require dirname(__FILE__) . '/../config.php';
require dirname(__FILE__) . '/../functions.php';
require dirname(__FILE__) . '/../db.php';
require dirname(__FILE__) . '/../amocrm.php';

//смена статуса сделки
if (isset($_REQUEST['leads']['status'][0]['id']) && isset($_REQUEST['leads']['status'][0]['status_id'])) {

    $leadID   = (int) $_REQUEST['leads']['status'][0]['id'];
    $statusID = (int) $_REQUEST['leads']['status'][0]['status_id'];

    changeLeadStatus($statusID, $leadID);

    //сделка закрыта и не реализована
    if ($statusID == 143) {

        //получаем ID родительской сделки из БД
        $mainLeadID = getMainLeadID($leadID);
        $mainLeadID = $mainLeadID['main_lead_id'];
        if (!empty($mainLeadID)) {
            //авторизуемся в AmoCRM
            if (auth()) {
                //получаем данные по текущей автоматически созданной сделке
                $currentLeadInfo = getCurrentLeadInfo($leadID);
                $causeDenial = ''; //текст причины отказа
                if (!empty($currentLeadInfo[0]['custom_fields'])) {
                    foreach ($currentLeadInfo[0]['custom_fields'] as $customField) {
                        if ($customField['id'] == 481797) {
                            $causeDenial = $customField['values'][0]['value'];
                        }
                    }
                }
                //создаем примечание к сделке
                $time = time();
                $note = "Ссылка на закрытую сделку: https://" . $subdomain . ".amocrm.ru/leads/detail/" . $leadID . "\r\n";
                if (!empty($causeDenial)) {
                    $note .= "Причина отказа: " . $causeDenial;
                }
                if ($createNoteInfo = createNote($mainLeadID, $note, $time)) {
                    //создаем задачу
                    $task = "Доработать заявку";
                    $createTaskInfo = createTask($mainLeadID, $task);
                }
            }
        }
    }
}

//удаление сделки
if (isset($_REQUEST['leads']['delete'][0]['id'])) {

    $leadID = (int) $_REQUEST['leads']['delete'][0]['id'];

    deleteLeadDB($leadID);
}
