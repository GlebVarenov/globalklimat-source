-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 22 2019 г., 16:28
-- Версия сервера: 5.7.23-24
-- Версия PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `u0463150_amo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `leads_applications`
--

CREATE TABLE IF NOT EXISTS `leads_applications` (
  `id` int(11) unsigned NOT NULL,
  `main_pipeline_id` int(11) unsigned NOT NULL COMMENT 'ID главной воронки, из которой создавали заявку и автоматическую сделку',
  `main_lead_id` int(11) unsigned NOT NULL COMMENT 'ID сделки, которой является главной по отношению к автоматической сделке',
  `child_pipeline_id` int(11) unsigned NOT NULL COMMENT 'ID воронки, в которой создали автоматическую сделку',
  `child_pipeline_name` varchar(255) NOT NULL,
  `child_status_id` int(11) unsigned NOT NULL COMMENT 'ID этапа, в котором создается автоматическая сделка',
  `child_lead_id` int(11) unsigned NOT NULL COMMENT 'ID автоматически созданной сделки',
  `child_lead_name` varchar(255) NOT NULL COMMENT 'название автоматической сделки',
  `child_lead_link` varchar(255) NOT NULL,
  `child_create_date` int(11) unsigned NOT NULL COMMENT 'дата создания автоматической сделки',
  `child_return_date` int(11) unsigned NOT NULL COMMENT 'дата возврата заявки, если она обработана'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `leads_applications`
--
ALTER TABLE `leads_applications`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `leads_applications`
--
ALTER TABLE `leads_applications`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
