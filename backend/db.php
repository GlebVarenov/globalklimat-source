<?php

/**
 * Соединяется с БД
 */
function connectDB()
{
    static $db_connections = [];
    $db_filename = getenv('DB_FILE');
    if (!empty($db_connections[$db_filename])) {
        $db = $db_connections[$db_filename];
        // проверяем соединение
        $result = $db->exec("SELECT `name` FROM `sqlite_master` WHERE `type` = 'table' AND `name` NOT LIKE 'sqlite_%';");
        if ($result) {
            return $db;
        }
    };

    $db = new \SQLite3($db_filename);
    $db->busyTimeout(5 * 1000);
    $db_connections[$db_filename] = $db;
    return $db;
}

/**
 * Сохраняет данные в БД в таблице leads_applications о главной сделке и об автоматически созданной дочерней
 * сделке, в которой создана заявка
 */
function saveLeadsApplications($mainPipelineID, $mainLeadID, $childPipelineID, $childPipelineName, $childStatusID, $childLeadID, $childLeadName, $time, $subdomain)
{
    $link = "https://" . $subdomain . ".amocrm.ru/leads/detail/" . $childLeadID;
    $db = connectDB();
    $q = "
        INSERT INTO leads_applications (main_pipeline_id, main_lead_id, child_pipeline_id, child_pipeline_name, child_status_id, child_lead_id, child_lead_name, child_lead_link, child_create_date, child_return_date) 
        VALUES ($mainPipelineID, $mainLeadID, $childPipelineID, '$childPipelineName', $childStatusID, $childLeadID, '$childLeadName', '$link', $time, 0); 
    ";

    if (!$db->query($q)) {
        recordLog("111", "Не удалось сохранить данные в БД.");
        return false;
    }

    return true;
}

/**
 * Получает уже имеющиеся заявки из БД для текущей главной сделки
 * @param $pipelineID
 * @param $leadID
 * @return array
 */
function getDBApplications($pipelineID, $leadID)
{
    $applications = array();
    $db = connectDB();
    $q = "SELECT child_pipeline_id, child_pipeline_name, child_lead_name, child_lead_link, child_create_date, child_status_id, child_return_date FROM leads_applications WHERE main_pipeline_id=$pipelineID AND main_lead_id=$leadID";
    if ($result = $db->query($q)) {
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $applications[] = $row;
        }
        if (!empty($applications)) {
            foreach ($applications as $key => $application) {
                $applications[$key]['child_create_date'] = date("d.m.Y H:i:s", $application['child_create_date']);
                if ($application['child_return_date'] != '0') {
                    $applications[$key]['child_return_date'] = date("d.m.Y H:i:s", $application['child_return_date']);
                }
            }
        }
    } else {
        recordLog("111", "Не удалось получить заявки из БД.");
    }

    return $applications;
}

/**
 * Получает данные о заявке для автоматически созданной сделки
 */
function getDBApplication($childPipelineID, $childLeadID)
{
    $application = array();
    $db = connectDB();
    $q = "SELECT main_lead_id FROM leads_applications WHERE child_pipeline_id=$childPipelineID AND child_lead_id=$childLeadID";
    if ($result = $db->query($q)) {
        $application = $result->fetchArray(SQLITE3_ASSOC);
    } else {
        recordLog("111", "Не удалось получить заявку из БД.");
    }

    return $application;
}

/**
 * Изменяет этап сделки и дату ее возврата в БД при изменении сделки клиентом в AmoCRM если она
 * перешла в этап "Закрыта и не реализована"
 */
function changeLeadStatus($statusID, $leadID)
{
    $db = connectDB();
    $q = "UPDATE leads_applications SET child_status_id=$statusID WHERE child_lead_id=$leadID";
    if (!$db->query($q)) {
        recordLog("111", "Не удалось обновить данные в БД при смене этапа сделки.");
    }
}

/**
 * Получает ID родительской сделки
 */
function getMainLeadID($id)
{
    $db = connectDB();
    $leadID = 0;
    $q = "SELECT main_lead_id FROM leads_applications WHERE child_lead_id=$id";
    if ($result = $db->query($q)) {
        $leadID = $result->fetchArray(SQLITE3_ASSOC);
    } else {
        recordLog("111", "Не удалось получить ID главной сделки.");
    }
    return $leadID;
}

/**
 * Обновляет время возврата файла
 */
function updateTimeFile($mainLeadID, $leadID, $time)
{
    $db = connectDB();
    $q = "UPDATE leads_applications SET child_return_date=$time WHERE main_lead_id=$mainLeadID AND child_lead_id=$leadID";
    if (!$db->query($q)) {
        recordLog("111", "Не удалось обновить время возврата файла.");
    }
}

/**
 * Удаляет сделку из БД
 */
function deleteLeadDB($id)
{
    $db = connectDB();
    $q = "DELETE FROM leads_applications WHERE child_lead_id=$id";
    if (!$db->query($q)) {
        recordLog("111", "Не удалось удалить сделку из БД.");
    }
}
