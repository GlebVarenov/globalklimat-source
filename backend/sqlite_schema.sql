-- создание таблице в SQL версии sqlite3 
CREATE TABLE IF NOT EXISTS `leads_applications` (
  `id` INTEGER PRIMARY KEY,
  `main_pipeline_id` unsigned int(11) NOT NULL, -- ID главной воронки, из которой создавали заявку и автоматическую сделку
  `main_lead_id` unsigned int(11) NOT NULL, -- ID сделки, которой является главной по отношению к автоматической сделке
  `child_pipeline_id` unsigned int(11) NOT NULL, -- ID воронки, в которой создали автоматическую сделку
  `child_pipeline_name` varchar(255) NOT NULL,
  `child_status_id` unsigned int(11) NOT NULL, -- ID этапа, в котором создается автоматическая сделка
  `child_lead_id` unsigned int(11) NOT NULL, -- ID автоматически созданной сделки
  `child_lead_name` varchar(255) NOT NULL, -- название автоматической сделки
  `child_lead_link` varchar(255) NOT NULL,
  `child_create_date` unsigned int(11) NOT NULL, -- дата создания автоматической сделки
  `child_return_date` unsigned int(11) NOT NULL -- дата возврата заявки, если она обработана
);
PRAGMA journal_mode=WAL; -- работает быстрее